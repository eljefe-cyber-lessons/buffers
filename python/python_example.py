#!/usr/bin/env python3
def anything(s):
	print("in function", s)

def main():
	s = 'hello world'
	anything(s)
	print(s)

if __name__ == '__main__':
	main()
