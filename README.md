## Buffers
The repository contains code -- primarily for buffer overflows and assembly. But additional demonstration code when I go off on a tangent.

### Assembly
Demonstration and building of assembly to run, jump and call functions.

### C
Demonstration of what the `-lm` and `-lpthread` flags are for when compiling.

### Python
Demonstration of Python3 in relation to Rust.

### Rust
Demonstration of the borrow check and move semantics.

Use `cargo run` to build and execute.
