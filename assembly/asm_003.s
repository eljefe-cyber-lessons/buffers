.text
.global _start
_start:
	xor %rax, %rax
	add $1, %rax
	call function
	jmp _start

function:
	mov $5, %rax
	mov $2, %rbx
	add %rax, %rbx
	ret

shellcode:
	xor %rax, %rax
	mov $0xd3adb33f, %rax
	jmp shellcode
