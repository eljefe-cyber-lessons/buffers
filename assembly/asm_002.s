.text
.global _start
_start:
	xor %rax, %rax
	add $1, %rax
	call function
	jmp _start

function:
	mov $5, %rax
	mov $2, %rbx
	add %rax, %rbx
	ret
