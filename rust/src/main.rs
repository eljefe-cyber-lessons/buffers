fn anything(s: String) {
    println!("in function: {}", s);
}

fn main() {
    let s = String::from("hello world");
    anything(s);
    println!("{}", s);
}
